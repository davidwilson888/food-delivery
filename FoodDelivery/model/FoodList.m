//
//  FoodList.m
//  FoodDelivery
//
//  Created by STUDENT on 9/25/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "FoodList.h"

@implementation FoodList {
    NSArray *foodList;
}

- (id)initWithArray:(NSArray *)foodArray setFoodType:(NSString *)foodType{
    self = [super init];
    if (self) {
        self.foodType = foodType;
        foodList = foodArray;
    }
    return self;
}

- (NSString *)getFoodNameAtIndex:(NSUInteger)index {
    return foodList[index];
}

- (NSUInteger)count {
    return [foodList count];
}

@end
