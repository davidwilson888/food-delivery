//
//  FoodMenu.h
//  FoodDelivery
//
//  Created by STUDENT on 9/25/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodMenu : NSObject

- (id)initWithArray:(NSArray *)foodListArray;
- (NSString *)getFoodNameByType:(NSString *)foodType atIndex:(NSUInteger)index;
- (NSString *)getFoodNameByTypeNumber:(NSUInteger)foodTypeNumber atIndex:(NSUInteger)index;
- (NSString *)getFoodTypeByTypeNumber:(NSUInteger)foodTypeNumber;
- (NSUInteger)countFoodList;
- (NSUInteger)countByFoodType:(NSString *)foodType;
- (NSUInteger)countByFoodTypeNumber:(NSUInteger)foodTypeNumber;

@end
