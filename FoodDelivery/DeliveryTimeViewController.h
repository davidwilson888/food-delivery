//
//  DeliveryTimeViewController.h
//  FoodDelivery
//
//  Created by STUDENT on 9/25/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeliveryTimeViewController : UIViewController

@property (strong, nonatomic) NSArray *selectedFoodName;

@end
