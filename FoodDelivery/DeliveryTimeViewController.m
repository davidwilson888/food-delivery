//
//  DeliveryTimeViewController.m
//  FoodDelivery
//
//  Created by STUDENT on 9/25/15.
//  Copyright (c) 2015 Sittipong Suwannatrai. All rights reserved.
//

#import "DeliveryTimeViewController.h"

@interface DeliveryTimeViewController ()
@property (weak, nonatomic) IBOutlet UILabel *selectedFoodLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *deliveryTimePicker;
@property (weak, nonatomic) IBOutlet UILabel *deliveryTimeLabel;

@end

@implementation DeliveryTimeViewController {
    NSString *deliveryTime;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)setupInitializeState {
    [self updateDeliveryTimeLabel];
    [self.deliveryTimePicker addTarget:self action:@selector(updateDeliveryTimeLabel) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated {
    self.selectedFoodLabel.text = [NSString stringWithFormat:@"%@ + %@ + %@", self.selectedFoodName[0], self.selectedFoodName[1], self.selectedFoodName[2]];
    NSLog(@"Food Selected: %@", [NSString stringWithFormat:@"%@ + %@ + %@", self.selectedFoodName[0], self.selectedFoodName[1], self.selectedFoodName[2]]);
    
    [self setupInitializeState];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateDeliveryTimeLabel {
    self.deliveryTimeLabel.text = [self getPickerTime];
}

- (NSString *) getPickerTime {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    return [dateFormatter stringFromDate:self.deliveryTimePicker.date];
}

- (void)showAlert
{
    self.deliveryTimeLabel.text = [NSString stringWithFormat:@"%@", deliveryTime];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Completed"
                                                    message:[NSString stringWithFormat:@"Thank you, your order will arrive\n%@", deliveryTime]
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStyleDefault;
    [alert show];
    [[self navigationController] popToRootViewControllerAnimated:YES];
}

- (IBAction)doneButtonTapped:(id)sender {
    deliveryTime = [self getPickerTime];
    self.deliveryTimeLabel.text = [NSString stringWithFormat:@"%@", deliveryTime];
    [self showAlert];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
